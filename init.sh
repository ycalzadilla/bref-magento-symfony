#!/usr/bin/env bash

#Begin Functions
function bash_color_library {
  # see if it supports colors...
  ncolors=$(tput colors)
  # shellcheck disable=SC2034
  if test -n "$ncolors" && test $ncolors -ge 8; then

    bold="$(tput bold)"
    underline="$(tput smul)"
    standout="$(tput smso)"
    normal="$(tput sgr0)"
    black="$(tput setaf 0)"
    red="$(tput setaf 1)"
    green="$(tput setaf 2)"
    yellow="$(tput setaf 3)"
    blue="$(tput setaf 4)"
    magenta="$(tput setaf 5)"
    cyan="$(tput setaf 6)"
    white="$(tput setaf 7)"
  fi
}

bash_color_library
bash_colors=$(bash_color_library)
export bash_colors

function symfonyKernelChanges() {
  FILENAME="app/src/Kernel.php"
  if [[ -e ${FILENAME} ]]; then
    sed "/public function registerBundles(): iterable/Q" ${FILENAME} >>${FILENAME}_temp

    cat << 'EOL' >> ${FILENAME}_temp
      public function getLogDir()
      {
          // When on the lambda only /tmp is writeable
          if (getenv('LAMBDA_TASK_ROOT') !== false) {
              return '/tmp/log/';
          }

          return parent::getLogDir();
      }

      public function getCacheDir()
      {
          // When on the lambda only /tmp is writeable
          if (getenv('LAMBDA_TASK_ROOT') !== false) {
              return '/tmp/cache/'.$this->environment;
          }

          return parent::getCacheDir();
      }
EOL
    grep -A 9999 "public function registerBundles(): iterable" ${FILENAME} >>${FILENAME}_temp
    mv ${FILENAME}_temp ${FILENAME}
  else
    echo "Cannot find Symfony kernel to update storage folder for serverless"
  fi
}

function magento_install {
  # Install magento
  echo "${blue}${bold}Installing magento community${normal}"
  export COMPOSER_AUTH='{"http-basic":{"repo.magento.com": {"username": "80426774d2fc8252e2835822636a0577", "password": "b1113208bc56e4855b0542da3eb7e79d"}}}'
  composer create-project --repository-url=https://repo.magento.com/ magento/project-community-edition app
}

function symfony_install {
  echo ""
  echo "${blue}${bold}Please, enter the Symfony skeleton version:"
  PS3='Select skeleton (1-2): '
  select skelVersion in 4.* 5.*;
  do [[ ${skelVersion} ]] && { break;} || echo Try again; done

  # Symfony Skeleton installation
  echo "${blue}${bold}Installing symfony skeleton and bref package${normal}"
  composer create-project symfony/skeleton:${skelVersion} app

  #Change kernel file storage
  symfonyKernelChanges
}
#End Functions

#Begin Main script
#Remove git files if they exits
echo "${blue}${bold}Removing Git Data!${normal}"
rm -rf ./.git/

# Setting options
echo ""
echo "${blue}${bold}Please, enter the Serverless service name [starterkit]:"
read -p "> " serviceName
[[ ${serviceName} ]] && serviceName=${serviceName} || serviceName='starterkit'

echo ""
echo "Please, enter the Framework to deploy:"
PS3='Select a framework (1-2): '
select framework in Symfony Magento;
do [[ ${framework} ]] && { break;} || echo Try again; done


if [[ ${framework} = "Magento" ]]; then
  # Magento stuff
  magento_install
else
  # Symfony stuff
  symfony_install
fi

#Install Bref
COMPOSER_MEMORY_LIMIT=-1 composer --working-dir=app require bref/bref
cp -rf app/. ./
rm -rf app/

#modify serverless file accordingly
sed -i "s/##service##/$serviceName/g" ./serverless.yml

#setup gitlab CI file
mv ./.gitlab-ci.yml.example ./.gitlab-ci.yml

echo "Project '$serviceName' setup correctly!"
rm ./init.sh