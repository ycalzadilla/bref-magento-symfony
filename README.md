
# Symfony and Magento starterkit for PHP AWS lambda serverless deployment   

---

Project using the [Serverless Framework](https://serverless.com), [Bref](https://bref.sh/), AWS Lambda, AWS API Gateway.

---

## Deployment

### Secrets

Secrets are injected into your functions using environment variables. By defining variables in the provider section of the `serverless.yml` you add them to the environment of the deployed function. From there, you can reference them in your functions as well.

So you would add something like:
```yml
provider:
  environment:
    A_VARIABLE: ${env:A_VARIABLE}
```
to your `serverless.yml`, and then you can add `A_VARIABLE` to your GitLab Ci variables and it will get picked up and deployed with your function.

For local development, we suggest installing something like [dotenv](https://www.npmjs.com/package/dotenv) to manage environment variables.

### Setting Up AWS

1. Create AWS credentials including the following IAM policies: `AWSLambdaFullAccess`, `AmazonAPIGatewayAdministrator` and `AWSCloudFormationFullAccess`.
1. Set the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` variables in the GitLab CI/CD settings. `Settings > CI/CD > Variables`.

## Development

### Running Locally

Install serverless dependencies with:

```sh
npm install
```

### Deploying

```sh
npm deploy -- --stage prod --region eu-central-1
```

### Docker local development environment

Docker environment that works with the same characteristics that the AWS Lambda function provides. To create it use:

```sh
docker compose up -d
```

After running docker-compose up, the application will be available at http://localhost:8000/.

Xdebug and Blackfire are supported for debugging and profiling.

Enjoy it!!